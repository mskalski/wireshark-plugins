do
  -- create myproto protocol and its fields
  p_isakmp_ext = Proto ("isakmp_ext", "ISAKMP")

  local exchange_types = {
    [0]  = "NONE",
    [1]  = "Base",
    [2]  = "Identity Protection (Main Mode)",
    [3]  = "Authentication Only",
    [4]  = "Aggressive",
    [5]  = "Informational_v1",
    [6]  = "Transaction (Config Mode)",
    [32] = "Quick Mode",
    [33] = "New Group Mode",

    [34] = "IKE_SA_INIT",
    [35] = "IKE_AUTH",
    [36] = "CREATE_CHILD_SA",
    [37] = "INFORMATIONAL",
    [38] = "IKE_SESSION_RESUME",
  }

  local payload_types = {
    [0 ] = "NONE/No next payload",
    [1 ] = "IKE_SA",
    [2 ] = "IKE_P",
    [3 ] = "IKE_T",
    [4 ] = "IKE_KE",
    [5 ] = "IKE_ID",
    [6 ] = "IKE_CERT",
    [7 ] = "IKE_CR",
    [8 ] = "IKE_HASH",
    [9 ] = "IKE_SIG",
    [10] = "IKE_NONCE",
    [11] = "IKE_N",
    [12] = "IKE_D",
    [13] = "IKE_VID",
    [14] = "IKE_A",
    [15] = "IKE_SAK",
    [16] = "IKE_SAT",
    [17] = "IKE_KD",
    [18] = "IKE_SEQ",
    [19] = "IKE_POP",
    [20] = "IKE_NAT_D",
    [21] = "IKE_NAT_OA",
    [22] = "IKE_GAP",
    [33] = "IKE2_SA",
    [34] = "IKE2_KE",
    [35] = "IKE2_IDI",
    [36] = "IKE2_IDR",
    [37] = "IKE2_CERT",
    [38] = "IKE2_CERTREQ",
    [39] = "IKE2_AUTH",
    [40] = "IKE2_NONCE",
    [41] = "IKE2_N",
    [42] = "IKE2_D",
    [43] = "IKE2_V",
    [44] = "IKE2_TSI",
    [45] = "IKE2_TSR",
    [46] = "IKE2_SK",
    [47] = "IKE2_CP",
    [48] = "IKE2_EAP",
    [49] = "IKE2_GSPM",
    [50] = "IKE2_IDG",
    [51] = "IKE2_GSA",
    [52] = "IKE2_KD",
    [53] = "IKE2_SKF",
    [130] = "IKE_NAT_D13",
    [131] = "IKE_NAT_OA14",
    [132] = "IKE_CISCO_FRAG",
  }

  local f_isakmp_header = ProtoField.bytes("isakmp_ext.orig_header", "Original ISAKMP Header")
  local f_cookie_i      = ProtoField.bytes("isakmp_ext.spi_i", "Initiator Cookie")
  local f_cookie_r      = ProtoField.bytes("isakmp_ext.spi_r", "Responder Cookie")
  local f_spi_i         = ProtoField.bytes("isakmp_ext.spi_i", "Initiator SPI")
  local f_spi_r         = ProtoField.bytes("isakmp_ext.spi_r", "Responder SPI")
  local f_next_payload  = ProtoField.uint8("isakmp_ext.type", "Next Payload", base.DEC, payload_types)
  local f_version       = ProtoField.string("isakmp_ext.version", "Version")
  local f_ver_mjr       = ProtoField.uint8("isakmp_ext.ver_mjr", "ISAKMP major version", base.HEX, {[1] = "IKEv1", [2] = "IKEv2"}, 0xf0)
  local f_ver_mnr       = ProtoField.uint8("isakmp_ext.ver_mnr", "ISAKMP minor version", base.HEX, nil, 0x0f)
  local f_exchange_type = ProtoField.uint8("isakmp_ext.exch", "Exchange type", base.DEC, exchange_types)
  local f_flags         = ProtoField.uint8("isakmp_ext.flags", "Flags", base.HEX)
  local f_flag_e        = ProtoField.uint8("isakmp_ext.flags.e", "Encryption", base.HEX, {[0] = 'Not encrypted', [1] = 'Encrypted'}, 0x01)
  local f_flag_c        = ProtoField.uint8("isakmp_ext.flags.a", "Commit", base.HEX, {[0] = 'No commit', [2] = 'Commited'}, 0x02)
  local f_flag_a        = ProtoField.uint8("isakmp_ext.flags.c", "Authentication", base.HEX, {[0] = 'Not authenticated', [2] = 'Authenticated'}, 0x04)
  local f_flag_i        = ProtoField.uint8("isakmp_ext.flags.i", "Initiator", base.HEX, {[0] = 'Initiator', [1] = 'Responder'}, 0x08)
  local f_flag_v        = ProtoField.uint8("isakmp_ext.flags.v", "Version", base.HEX, {[0] = 'No higher version', [2] = 'Higher version available'}, 0x10)
  local f_flag_r        = ProtoField.uint8("isakmp_ext.flags.r", "Response", base.HEX, {[0] = 'Request', [2] = 'Response'}, 0x20)
  local f_msgid         = ProtoField.uint32("isakmp_ext.msgid", "Message ID", base.HEX)
  local f_length        = ProtoField.uint32("isakmp_ext.length", "Message Length", base.DEC)

  p_isakmp_ext.fields = {f_isakmp_header, f_cookie_i, f_cookie_r, f_next_payload,
                         f_version, f_ver_mjr, f_ver_mnr, f_exchange_type,
                         f_flags, f_flag_e, f_flag_c, f_flag_a, f_flag_i, f_flag_v, f_flag_r,
                         f_msgid, f_length, f_spi_i, f_spi_r, }

  -- create a Boolean preference named "bar" for Foo Protocol
  -- (assuming Foo doesn't already have a preference named "bar")
  p_isakmp_ext.prefs.force_unencrypted = Pref.bool( "Decode as not encrypted packet", false, "Ignore ENCRYPTED flag for ISAKMP v1 packets" )
  p_isakmp_ext.prefs.decode_header = Pref.bool( "Show original ISAKMP header", true, "Decode original IKE Header")


  local ike_dissector
  function call_ike_dissector(tvbuf, pinfo, treeitem)
    if ike_dissector then 
      return pcall(ike_dissector.call, ike_dissector, tvbuf, pinfo, treeitem)
    end
  end

  -- myproto dissector function
  function p_isakmp_ext.dissector (tvbuf, pinfo, treeitem)
    -- validate packet length is adequate, otherwise quit
    if tvbuf:len() >= 28 then --and tvbuf(17,1):bitfield(0,4) == 1 then

      local ver = tvbuf(17,1):uint()
      local enc = tvbuf(19,1):uint() % 2
      local enc_str = ""

      pinfo.cols.protocol = p_isakmp_ext.name

      local subtree = treeitem:add(p_isakmp_ext, tvbuf(0))

      if p_isakmp_ext.prefs.decode_header then
        -- create subtree for ikeext
        if (enc ~= 0) then
          enc_str = " (Encrypted packet)"
        else
          enc_str = " (Unencrypted packet)"
        end

        local header = subtree:add(f_isakmp_header, tvbuf(0,28), '', 'ISAKMP original header'..enc_str)

        -- add protocol fields to subtree
        if ((ver/16) == 1) then
          header:add(f_cookie_i, tvbuf(0,8))
          header:add(f_cookie_r, tvbuf(8,8))
        else
          header:add(f_spi_i, tvbuf(0,8))
          header:add(f_spi_r, tvbuf(8,8))
        end
        header:add(f_next_payload, tvbuf(16,1))
        local version = header:add(f_version, tvbuf(17,1), ver, "Version: "..(ver / 16).."."..(ver % 16))
        version:add(f_ver_mjr, tvbuf(17,1))
        version:add(f_ver_mnr, tvbuf(17,1))
        header:add(f_exchange_type, tvbuf(18,1))
        local flags = header:add(f_flags, tvbuf(19,1))
        if ((ver/16) == 1) then
          flags:append_text(enc_str)
          flags:add(f_flag_e, tvbuf(19,1))
          flags:add(f_flag_c, tvbuf(19,1))
          flags:add(f_flag_a, tvbuf(19,1))
        else
          flags:add(f_flag_i, tvbuf(19,1))
          flags:add(f_flag_v, tvbuf(19,1))
          flags:add(f_flag_r, tvbuf(19,1))
        end
        header:add(f_msgid, tvbuf(20,4))
        header:add(f_length, tvbuf(24,4))
      end

      if p_isakmp_ext.prefs.force_unencrypted then
        if ((ver/16) == 1) then
          if (enc ~= 0) then
            -- description of payload
            subtree:append_text(" - decoding as unencrypted ISAKMP packet")
            -- replace encryped flag with 0
            local new_data = tvbuf(0):bytes()
            new_data:set_index(19, 0) -- FIXME: get values of other flags into account
            call_ike_dissector(new_data:tvb(), pinfo, subtree)
            return
          else
            subtree:append_text(" - unencrypted ISAKMP packet")
            call_ike_dissector(tvbuf, pinfo, subtree)
            return
          end
        else
          subtree:append_text(" - forcing unencrypted not spupported for IKEv2")
        end
      else
        
      end
    end

    call_ike_dissector(tvbuf, pinfo, treeitem)
  end

  -- Initialization routine
  function p_isakmp_ext.init()
  end

  -- register a chained dissector for port 500
  local udp_dissector_table = DissectorTable.get("udp.port")

  ike_dissector = udp_dissector_table:get_dissector(500)
  if not ike_dissector then
    local tcp_dissector_table = DissectorTable.get("tcp.port")
    ike_dissector = tcp_dissector_table:get_dissector(500)
  end

  udp_dissector_table:add(500, p_isakmp_ext)
end
