-- Dissector for parsing ESP unencrypted data (ESP skeleton mode)

local debug_level = {
    DISABLED = 0,
    LEVEL_1  = 1,
    LEVEL_2  = 2
}

-- set this DEBUG to debug_level.LEVEL_1 to enable printing debug_level info
-- set it to debug_level.LEVEL_2 to enable really verbose printing
-- note: this will be overridden by user's preference settings
local DEBUG = debug_level.LEVEL_1

local default_settings =
{
    debug_level  = DEBUG,
    enable       = true,
    iv_len       = 16,
    icv_len      = 16,
}

local dprint = function() end
local dprint2 = function() end
local function reset_debug_level()
    if default_settings.debug_level > debug_level.DISABLED then
        dprint = function(...)
            print(table.concat({"Lua:", ...}," "))
        end

        if default_settings.debug_level > debug_level.LEVEL_1 then
            dprint2 = dprint
        end
    end
end
-- call it now
reset_debug_level()

dprint2("Wireshark version = ", get_version())
dprint2("Lua version = ", _VERSION)


----------------------------------------
-- creates a Proto object, but doesn't register it yet
local esp_skel = Proto("esp_skel", "Encapsulating Security Payload (skeleton mode)")

-- mirror fields from ESP protocol
local f_spi        = ProtoField.uint32("esp.spi",      "ESP SPI",     base.HEX_DEC)
local f_seq        = ProtoField.uint32("esp.sequence", "ESP Seqence", base.DEC)
local f_iv         = ProtoField.bytes("esp.iv",        "ESP IV")
local f_encdata    = ProtoField.bytes("esp.encrypted_data", "ESP Encrypted Data")
local f_icv        = ProtoField.bytes("esp.icv",       "ESP ICV")
local f_icv_good   = ProtoField.bool("esp.icv_good",   "Good")
local f_icv_bad    = ProtoField.bool("esp.icv_bad",    "Bad")

local f_decdata    = ProtoField.bytes("esp.decrypted_data", "ESP Decrypted Data")
local f_contdata   = ProtoField.bytes("esp.contained_data", "ESP Contained Data")
local f_pad        = ProtoField.bytes("esp.pad",       "Pad")
local f_padlen     = ProtoField.uint8("esp.pad_len",   "ESP Pad Length")
local f_esp_proto  = ProtoField.uint8("esp.protocol",  "ESP Next Header", base.HEX, {[4] = "IPv4", [41] = "IPv6"})

esp_skel.fields = {f_esp, f_spi, f_seq, f_iv, f_encdata, f_icv, f_icv_good, f_icv_bad,
                   f_decdata, f_contdata, f_pad, f_padlen, f_esp_proto}


local ipproto = DissectorTable.get("ip.proto")
local esp_dissector = ipproto:get_dissector(50)
local ip_dissector = Dissector.get("ip")


function esp_skel.init()
    ipproto:add(50, esp_skel)
end

local debug_pref_enum = {
    { 1,  "Disabled", debug_level.DISABLED },
    { 2,  "Level 1",  debug_level.LEVEL_1  },
    { 3,  "Level 2",  debug_level.LEVEL_2  },
}

esp_skel.prefs.debug = Pref.enum("Debug", default_settings.debug_level, "The debug printing level", debug_pref_enum)
esp_skel.prefs.enable = Pref.bool( "Try to decode as unencrypted packet", default_settings.enable, "Try to parse ESP data as not encrypted")
esp_skel.prefs.iv_len = Pref.uint( "Default size of initial IV data", default_settings.iv_len, "Size of IV data which shall be skipped before real unencrypted IP packet")
esp_skel.prefs.icv_len = Pref.uint( "Default size of trailing ICV data", default_settings.icv_len, "Size of ICV data which follows parsed unencrypted IP packet")



----------------------------------------
-- a function for handling prefs being changed
function esp_skel.prefs_changed()
    dprint2("prefs_changed called")

    default_settings.debug_level  = esp_skel.prefs.debug
    reset_debug_level()

    default_settings.enable  = esp_skel.prefs.enable
    default_settings.iv_len  = esp_skel.prefs.iv_len
    default_settings.icv_len = esp_skel.prefs.icv_len

end

dprint2("esp_skel prefs registered")



ipv4_packet_check = function(tvbuf)
    local sum = 0
    local hdrsum = 0
    local hdrlen
    local i=0
    local carry

    -- ipv4 header shall have minimum 20 bytes
    if tvbuf:len() < 20 then
        return -1
    end
    hdrlen = (tvbuf(0,1):uint() % 16) * 4
    if (tvbuf:len() < hdrlen) then
        return -1
    end

    while i < hdrlen do
        local a = tvbuf:range(i,2):uint()
        if i == 10 then
            hdrsum = a
            a=0
        end
        sum = sum + a
        i = i+2
    end

    carry = math.floor(sum / 65536)
    sum = (sum + carry) % 65536
    carry = math.floor(sum / 65536)
    sum = (sum + carry) % 65536

    sum = 65535 - sum

    dprint2("Header checksum:   " .. string.format("0x%04x", hdrsum))
    dprint2("Computed checksum: " .. string.format("0x%04x", sum))

    -- verify IPv4 header checksum
    if hdrsum ~= sum then
        print("Invalid IPv4 checksum")
        return 0
    end

    -- verify total length does not exceed buffer
    local total_length = tvbuf:range(2,2):uint()
    if total_length > tvbuf:len() then
        dprint("Invalid IPv4 data length")
        return 0
    end

    return total_length
end

local esp_get_pad_len = function(tvbuf)
    return tvbuf:range(tvbuf:len()-1, 1):uint()
end

local esp_proto2str = function(val)
    if val == 4 then
        return "IPv4"
    elseif val == 6 then
        return "IPv6"
    else
        return string.format("%d", val)
    end
end

local add_bytes_with_len = function(treeitem, proto, tvbuf)
    local newitem = treeitem:add(proto, tvbuf)

    local s
    if tvbuf:len() == 1 then
        s = " (1 byte)"
    else
        s = string.format(" (%d bytes)", tvbuf:len())
    end

    newitem:append_text(s)
    return newitem
end

----------------------------------------
-- The following creates the callback function for the dissector.
-- It's the same as doing "dns.dissector = function (tvbuf,pkt,root)"
-- The 'tvbuf' is a Tvb object, 'pinfo' is a Pinfo object, and 'root' is a TreeItem object.
-- Whenever Wireshark dissects a packet that our Proto is hooked into, it will call
-- this function and pass it these arguments for the packet it's dissecting.
function esp_skel.dissector(tvbuf, pinfo, root)

    local handled = false

    if default_settings.enable then
        local iv_len = default_settings.iv_len
        local icv_len = default_settings.icv_len

        -- TODO: Search of given SPI in ESP UAT. Not trivial in Lua

        local buflen = tvbuf:len()
        local ip_data = tvbuf(8+iv_len, buflen-8-iv_len-icv_len)

        local ipv4_length = ipv4_packet_check(ip_data)

        if ipv4_length > 0 then

            -- assume protocol is ESP skel
            pinfo.cols.protocol = esp_skel.name

            -- add ESP fields
            local header = root:add(esp_skel, tvbuf(0))

            if buflen > 8 then

                header:add(f_spi, tvbuf(0,4))
                header:add(f_seq, tvbuf(4,4))
                if iv_len + 8 < buflen then
                    add_bytes_with_len(header, f_iv,  tvbuf(8, iv_len))
                    if icv_len + iv_len + 8 < buflen then
                        local dec_tvbuf = tvbuf:range(8+iv_len, buflen-icv_len-iv_len-8)
                        add_bytes_with_len(header, f_encdata, dec_tvbuf)
                        local icv_item = add_bytes_with_len(header, f_icv, tvbuf(buflen-icv_len, icv_len))
                        icv_item:append_text(" [unchecked]")
                        icv_item:add(f_icv_good, tvbuf(buflen-icv_len, icv_len), false):set_generated()
                        icv_item:add(f_icv_bad, tvbuf(buflen-icv_len, icv_len), false):set_generated()

                        local dec_item = add_bytes_with_len(header, f_decdata, dec_tvbuf)
                        local pad_len = esp_get_pad_len(dec_tvbuf(0, dec_tvbuf:len()-1))
                        if (pad_len <= dec_tvbuf:len()-1) then
                            local cont_tvbuf = dec_tvbuf:range(0, dec_tvbuf:len() - pad_len - 2)
                            add_bytes_with_len(dec_item, f_contdata, cont_tvbuf)
                            dec_item:add(f_pad, dec_tvbuf(dec_tvbuf:len() - pad_len - 2, pad_len))
                            dec_item:add(f_padlen, dec_tvbuf(dec_tvbuf:len() - 2, 1))
                        end
                        -- last byte shall be Next protocol field
                        dec_item:add(f_esp_proto, dec_tvbuf(dec_tvbuf:len()-1, 1))
                    else
                        add_bytes_with_len(header, f_encdata, tvbuf(8+iv_len, buflen-iv_len-8))
                    end
                else
                    add_bytes_with_len(header, f_iv, tvbuf(8, buflen-8))
                end
            end

            -- try to parse ip protocol
            if ip_dissector:call(ip_data:tvb(), pinfo, root) then
                dprint2("Succesfully parsed IPv4 payload")
                handled = true
            end
        end
    end

    if not handled then
        if esp_dissector then
            esp_dissector:call(tvbuf, pinfo, root)
        end
        local header = root:add(esp_skel, tvbuf(0))
        header:set_text("<Encapsulating Security Payload (skeleton mode) settings>")
    end
end

